/*
 * SIMPLE NODEMCU FIRST IOT PROTOTYPE
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>  // This library is already built in to the Arduino IDE
#include <LiquidCrystal_PCF8574.h>


#define PIN_DS18B20 D4  // DS18B20 on arduino pin2 corresponds to D4 on physical board
#define PIN_LED D5
#define PIN_PUSHBUTTON D8

// For WIFI Connection
const char* MY_SSID = "WLAN_GRUPOCELSA"; 
const char* MY_PWD = "92856926466343656101744d3a";

// For MQTT Server
WiFiClient espClient;
PubSubClient client(espClient);
const char* MY_MQTT_SERVER = "10.1.210.37"; // CELSA RPI1
const int MY_MQTT_SERVER_PORT= 1883;

const char* MY_PUBLISH_TOPIC_IP="demoiot/ip";
const char* MY_PUBLISH_TOPIC_TEMP="demoiot/temperature";
const char* MY_PUBLISH_TOPIC_BUTTON="demoiot/button";
const char* MY_PUBLISH_TOPIC_TEMP_THINGSPEAK="channels/213598/publish/fields/field1/5O03Z0069CFAGMPZ";

const char* MY_SUBSCRIPTION_TOPIC_LED="demoiot/led";
const char* MY_SUBSCRIPTION_TOPIC_LCD="demoiot/lcd";

unsigned long last_published_time = 0; 
const unsigned long MQTT_PUBLISH_RATE = 60L * 1000L; 




// For LCD
LiquidCrystal_PCF8574 lcd(0x27);  // set the LCD address to 0x27 for a 16 chars and 2 line display
String LCD_Line1;
String LCD_Line2;
const unsigned int LCD_REFRESH_RATE = 500;
static unsigned long last_lcd_refresh_time = 0;


// For Tempererature Sensor DS18B20 
OneWire oneWire(PIN_DS18B20);
DallasTemperature DS18B20(&oneWire);
const unsigned int TEMPERATURE_CAPTURE_RATE = 1000;
static unsigned long last_temperature_refresh_time = 0;

String LedStatus;
String PushButtonStatus;
String Temperature;
String Message;
String Statuses[] =  { "WL_IDLE_STATUS=0", "WL_NO_SSID_AVAIL=1", "WL_SCAN_COMPLETED=2", "WL_CONNECTED=3", "WL_CONNECT_FAILED=4", "WL_CONNECTION_LOST=5", "WL_DISCONNECTED=6"};

// Push button
static unsigned long last_interrupt_time = 0;
static unsigned long last_pushed = 0;
unsigned int PushButtonNotified = 0;
const unsigned int DEBOUNCE_TIME = 500;
const unsigned int BUTTON_REFRESH_TIME = 3000;

void setup() {

  //Initialize Serial output
  Serial.begin(115200);
  Serial.println("-----------SETUP - BEGIN------------");
  Serial.println("NodeMcu IOT");
  Serial.println("-----------");
  Serial.println("Configuration Parameters:");
  Serial.printf(" * MY_MQTT_SERVER            : %s \r\n",MY_MQTT_SERVER);
  Serial.printf(" * MY_MQTT_SERVER_PORT       : %i \r\n",MY_MQTT_SERVER_PORT);
  Serial.printf(" * MY_PUBLISH_TOPIC_IP       : %s \r\n",MY_PUBLISH_TOPIC_IP);
  Serial.printf(" * MY_PUBLISH_TOPIC_TEMP     : %s \r\n",MY_PUBLISH_TOPIC_TEMP);
  Serial.printf(" * MY_PUBLISH_TOPIC_BUTTON   : %s \r\n",MY_PUBLISH_TOPIC_BUTTON);
  Serial.printf(" * MY_SUBSCRIPTION_TOPIC     : %s \r\n",MY_SUBSCRIPTION_TOPIC_LED);
  Serial.printf(" * MY_SSID                   : %s \r\n",MY_SSID);
  Serial.println("Pinouts:");  
  Serial.printf(" * LED                       : %i \r\n",PIN_LED);
  Serial.printf(" * PUSH BUTTON               : %i \r\n",PIN_PUSHBUTTON);
  Serial.printf(" * DS18B20                   : %i \r\n",PIN_DS18B20);
  Serial.println("Constants:");  
  Serial.printf(" * LCD_REFRESH_RATE          : %i \r\n",LCD_REFRESH_RATE);
  Serial.printf(" * TEMPERATURE_CAPTURE_RATE  : %i \r\n",TEMPERATURE_CAPTURE_RATE);
  Serial.printf(" * BUTTON_REFRESH_TIME       : %i \r\n",BUTTON_REFRESH_TIME);
  
  // LCD setup begin ----------------------------------------------------------------------
  int error;
  Wire.begin();
  Wire.beginTransmission(0x27);
  error = Wire.endTransmission();
  Serial.print("Error: ");
  Serial.print(error);
  if (error == 0) {
    Serial.println(": LCD found.");
  } else {
    Serial.println(": LCD not found.");
  } // if

  lcd.begin(16, 2); // initialize the lcd
  // LCD setup end ----------------------------------------------------------------------

  // LCD initial message
  Serial.println("Printing inital message to LCD...");
  Write_LCD("  *** IOT ***   ","  CELSA GROUP ");
  delay(3000);

  //Defining MQTT server
  Serial.println("Setting MQTT SERVER...");
  client.setServer(MY_MQTT_SERVER, MY_MQTT_SERVER_PORT);
  client.setCallback(Callback_Subscription_Received);

  // Configure and disable LED
  Serial.println("Configuring LED D2 as output. Setting to LOW.");
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, LOW);
  LedStatus = "0";

  // Configure Pushbutton and attach it to an interrupt.
  pinMode(PIN_PUSHBUTTON, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_PUSHBUTTON), PushButtonPressed, CHANGE);
  PushButtonStatus="0";
  
  Serial.println("-----------SETUP - END------------");
  Serial.println("");
}

void loop() {

  //Check if Wifi has connected else reconnect
  
  if (WiFi.status() != WL_CONNECTED)
  {
    Connect_To_Wifi();
  }
  
  // Check if MQTT client has connected else reconnect
  if (!client.connected()) 
  {
    mqtt_reconnect();
  }
    
  client.loop();

  if (millis() - last_temperature_refresh_time > TEMPERATURE_CAPTURE_RATE)
  {
    Temperature = String(Get_Temperature());
  }
  
 
  if (millis() - last_published_time > MQTT_PUBLISH_RATE || last_published_time  == 0) 
  {  
    mqtt_publish(MY_PUBLISH_TOPIC_IP,WiFi.localIP().toString());
    mqtt_publish(MY_PUBLISH_TOPIC_TEMP,Temperature);
    mqtt_publish(MY_PUBLISH_TOPIC_TEMP_THINGSPEAK,Temperature);

    mqtt_subscribe(MY_SUBSCRIPTION_TOPIC_LED);
    mqtt_subscribe(MY_SUBSCRIPTION_TOPIC_LCD); 
    
  }

  PushButtonReset();

  if (millis() - last_lcd_refresh_time > LCD_REFRESH_RATE)
  {
    Update_LCD();
  }
      
}

void Write_LCD(String Line1, String Line2)
{
    lcd.setBacklight(255); // Enable or Turn On the backlight  
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(Line1); // Start Print text to Line 1
    lcd.setCursor(0, 1);
    lcd.print(Line2); // Start Print text to Line 1
}

void Update_LCD()
{

  String LCD_Line1_New="TEMP:" + Temperature + "\337C" + " L:" + LedStatus;
  String LCD_Line2_New="P:" + PushButtonStatus + " M:" + Message ;

  if (LCD_Line1 != LCD_Line1_New || LCD_Line2 != LCD_Line2_New )
  {
    Serial.println("Updating LCD!!!");
    Write_LCD(LCD_Line1_New,LCD_Line2_New);
    LCD_Line1=LCD_Line1_New;
    LCD_Line2=LCD_Line2_New;
  }

  last_lcd_refresh_time = millis();
}

void PushButtonPressed()
{
  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > DEBOUNCE_TIME) {
   Serial.println("PushButton Pushed!");
   PushButtonStatus="1";
   last_pushed = millis();
  }
  else
  {
    Serial.println("PushButton is already pushed - ignoring push...");
  }
  last_interrupt_time = interrupt_time;
}

void PushButtonReset()
{
  if (PushButtonStatus=="1")
  {
    if (PushButtonNotified==0)
    {
      mqtt_publish(MY_PUBLISH_TOPIC_BUTTON,"Active");
      PushButtonNotified=1;
    }
    if (millis() - last_pushed > BUTTON_REFRESH_TIME)
    {
      Serial.println("Reseting push button status!");
      PushButtonStatus="0";
      PushButtonNotified=0;
      last_pushed=0;
    }
  }
}

float Get_Temperature()
{
  float tempaux;
  do {
    DS18B20.requestTemperatures(); 
    tempaux = DS18B20.getTempCByIndex(0);
    
    Serial.printf("Temperature [%s]\r\n",String(tempaux).c_str());
    if (tempaux == -127.00 or tempaux == 85.00)
    {
      Serial.printf("WARNING! - Incorrect value retrying...\r\n");
      delay(1000);
    }
  } while (tempaux == -127.00 or tempaux == 85.00);

  last_temperature_refresh_time = millis();

  return tempaux;
}

void Connect_To_Wifi()
{
  Write_LCD( MY_SSID, "Connecting..."); 
  Serial.printf("Connecting to WIFI SSID [%s].",MY_SSID);
  WiFi.begin(MY_SSID, MY_PWD);
  WiFi.mode(WIFI_STA);
  
  Serial.printf("Trying to connect until status [%s]...\r\n",Statuses[WL_CONNECTED].c_str());
  int r=1;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.printf("ERROR - Retrying due to STATUS is [%s].\r\n",Statuses[WiFi.status()].c_str());
    String MessageError="Wifi retry " + String(r) + "...";
    Write_LCD( MY_SSID,MessageError);
    delay(1000);
    r+=1;
  }
  Write_LCD(MY_SSID,WiFi.localIP().toString().c_str());
  Serial.printf("Connected! - Current IP address [%s].\r\n",WiFi.localIP().toString().c_str());
}

void mqtt_publish(String Topic, String Message)
{
  Serial.printf("MQTT Publish - Topic:[%s] - Msg:[%s]\r\n",Topic.c_str(),Message.c_str());
  client.publish(Topic.c_str(), Message.c_str());
  last_published_time = millis();
}

void mqtt_subscribe(String Topic)
{
  Serial.printf("MQTT Subscribe - Topic:[%s]\r\n",Topic.c_str());
  client.subscribe(Topic.c_str());
}

void mqtt_reconnect() 
{
  // Loop until we're reconnected
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    // Connect to the MQTT broker
    if (client.connect("ArduinoWiFi101Client")) 
    {
      Serial.println("connected");
    } else 
    {
      Serial.print("failed, rc=");
      // Print to know why the connection failed
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying to connect again
      delay(5000);
    }
  }
}


void Callback_Subscription_Received(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] length [");
  Serial.print(length);
  Serial.print("] payload [");
  String Msg="";
  for (int i = 0; i < length; i++) {
    Msg+=(char)payload[i];
    Serial.print((char)payload[i]);
  }
  Serial.println("]");

  if (String(topic) == String(MY_SUBSCRIPTION_TOPIC_LCD))
  {
    Message = Msg;
  }
  else if (String(topic) == String(MY_SUBSCRIPTION_TOPIC_LED))
  {
    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == '1') {
      digitalWrite(PIN_LED, HIGH);   
      LedStatus = "1";
    } else {
      LedStatus = "0";
      digitalWrite(PIN_LED, LOW);  // Turn the LED off by making the voltage HIGH
    }
  }
  else{
     Serial.println("Topic unknown...");
  }

  
}


