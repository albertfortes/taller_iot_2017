#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

const char* ssid = "WLAN_GRUPOCELSA"; 
const char* password = "92856926466343656101744d3a";

ESP8266WebServer server ( 80 );

const int PIN_LED = D5;

void setup ( void ) {
	pinMode ( PIN_LED, OUTPUT );

	Serial.begin ( 115200 );
	WiFi.begin ( ssid, password );
	Serial.println ( "" );

	// Wait for connection
	while ( WiFi.status() != WL_CONNECTED ) {
		delay ( 500 );
		Serial.print ( "." );
	}

	Serial.println ( "" );
	Serial.print ( "Connected to " );
	Serial.println ( ssid );
	Serial.print ( "IP address: " );
	Serial.println ( WiFi.localIP() );

	server.on ( "/", handleRoot );
	server.on ( "/ledon", []() {
    digitalWrite(PIN_LED,HIGH);
    server.send ( 200, "text/plain", "LED ON" );
  } );
	server.on ( "/ledoff", []() {
  digitalWrite(PIN_LED,LOW);
		server.send ( 200, "text/plain", "LED OFF" );
	} );
	server.onNotFound ( handleNotFound );
	server.begin();
	Serial.println ( "HTTP server started" );
}

void loop ( void ) {
	server.handleClient();
}

void handleRoot() {
  
  char temp[255];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;
  String Output="<!DOCTYPE html>\
  <html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
    <title>NODEMCU - TALLER IOT</title>\
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\">\
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" rel=\"stylesheet\">\
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>\
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\
  </head>\
      <body>\
      <div class=\"container\">\
      <div class=\"jumbotron\">\
      <h1>Hello from NodeMCU!</h1>\
      <p>Taller IOT - CELSA IT</p>\
      <p>\
      <div class=\"btn-group\" role=\"group\" aria-label=\"Led\">\
        <button id=\"B_ON\" type=\"button\" class=\"btn btn-danger\">ON</button>\
        <button id=\"B_OFF\" type=\"button\" class=\"btn btn-success\">OFF</button>\
      </div>\
      </p>\
      </div>\
      </div>\
    <script>\
    $( \"#B_ON\" ).click(function() {  $.get( \"/ledon\"); });\
    $( \"#B_OFF\" ).click(function() {  $.get( \"/ledoff\"); });\
    </script>\
  </body>\
</html>\
  ";

  server.send ( 200, "text/html", Output.c_str() );

}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }

  server.send ( 404, "text/plain", message );
  
}

