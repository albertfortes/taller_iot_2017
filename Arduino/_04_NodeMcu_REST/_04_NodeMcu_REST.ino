
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//Constants
#define myPeriodic 15 //in sec | Thingspeak pub min is 15sec
#define ONE_WIRE_BUS D4  // DS18B20 on arduino pin2 corresponds to D4 on physical board
const char* server = "api.thingspeak.com";
String apiKey ="E6BVNH5BAWR8TQMB";
const char* MY_SSID = "WLAN_GRUPOCELSA"; 
const char* MY_PWD = "92856926466343656101744d3a";

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);

String Statuses[] =  { "WL_IDLE_STATUS=0", "WL_NO_SSID_AVAIL=1", "WL_SCAN_COMPLETED=2", "WL_CONNECTED=3", "WL_CONNECT_FAILED=4", "WL_CONNECTION_LOST=5", "WL_DISCONNECTED=6"};

void setup() {

  Serial.begin(115200);
  Serial.println("NodeMcu IOT");
  Serial.println(" * ChipId:");
  Serial.println(ESP.getChipId());
  Serial.println("Configuration Parameters:");
  Serial.print(" * server:");
  Serial.println(server);
  Serial.print(" * MY_SSID:");
  Serial.println(MY_SSID);
}

void loop() {
  float temp;
  
  connectWifi();
  temp = GetTemperature();
  sendTeperatureTS(temp);
  //GoToDeepSleep(myPeriodic);
  delay(100);
  Serial.println("sleeping...");
  delay(myPeriodic*1000);
  
}

void connectWifi()
{
  Serial.print("Connecting to WIFI SSID [");
  Serial.print(MY_SSID);
  Serial.println("]");
  
  WiFi.begin(MY_SSID, MY_PWD);
  // Set WiFi mode to station (as opposed to AP or AP_STA)
  WiFi.mode(WIFI_STA);

  Serial.print("Trying to connect until status [");
  Serial.print(Statuses[WL_CONNECTED]);
  Serial.println("]");
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("RETRYING... -> STATUS [");
    Serial.print(Statuses[WiFi.status()]);
    Serial.println("]");
  }
  
  Serial.println("");
  Serial.print("Connected! - ");
  Serial.print("IP address [");
  Serial.print(WiFi.localIP());
  Serial.println("]");
}//end connect

float GetTemperature()
{
  float tempaux;
  do {
    DS18B20.requestTemperatures(); 
    tempaux = DS18B20.getTempCByIndex(0);
    Serial.print("Temperature [");
    Serial.print(tempaux);
    Serial.println("]");
    if (tempaux == -127.00 or tempaux == 85.00)
    {
      Serial.println("WARNING! - " + String(tempaux) + " retrying...");
      delay(1000);
    }
  } while (tempaux == -127.00 or tempaux == 85.00);

  return tempaux;
}

void sendTeperatureTS(float temp)
{  
   WiFiClient client;
  
   if (client.connect(server, 80)) {
   Serial.print("Connected to [");
   Serial.print(server);
   Serial.println("]");
   
   String postStr = apiKey;
   postStr += "&field1";
   postStr += "=";
   postStr += String(temp);
   postStr += "\r\n\r\n";
   
   client.print("POST /update HTTP/1.1\n");
   client.print("Host: api.thingspeak.com\n");
   client.print("Connection: close\n");
   client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
   client.print("Content-Type: application/x-www-form-urlencoded\n");
   client.print("Content-Length: ");
   client.print(postStr.length());
   client.print("\n\n");
   client.print(postStr);

   Serial.println("Data has been sent.");
   delay(1000);
   
   }//end if
   
 client.stop();
}//end send


