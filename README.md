## Synopsis

Material para taller de IOT. Incluye los códigos de Arduino útilizados para la presentación.

## References

* LCD - https://www.losant.com/blog/how-to-connect-lcd-esp8266-nodemcu
* Pushbutton - http://meandmyarduino.blogspot.com.es/2011/12/interrupts.html 

## Author

Albert Fortes (afortes@gcelsa.com)